from vector import Vector
import math


def angleToVector(p_angle: int):
    radians = p_angle * (math.pi / 180)
    return Vector(math.cos(radians), math.sin(radians))


def inBounds(p_pos: Vector, maxBounds: tuple, minBounds=Vector(0, 0)):
    x_inBounds = minBounds.x <= p_pos.x <= maxBounds[0]
    y_inBounds = minBounds.y <= p_pos.y <= maxBounds[1]

    return x_inBounds and y_inBounds
