from board import Board
from functions import *
import numpy


def calculateNRays(p_windowSize: tuple, p_board: Board, p_mouse_position: Vector, p_rayRange=(0, 360),
                   p_angle_precision=1, p_precision=1):
    rays = []

    minAngle = p_rayRange[0]
    maxAngle = p_rayRange[1]
    angles = numpy.linspace(minAngle, maxAngle, int((maxAngle - minAngle) * 1 / p_angle_precision))

    for i in angles:
        newMousePos = Vector(p_mouse_position.x, p_mouse_position.y)
        rays.append(calculateOneRay(p_windowSize, p_board, newMousePos, i, p_precision))

    return rays


def calculateOneRay(p_windowSize: tuple, p_board: Board, p_mouse_position: Vector, p_angle: int, precision=1):
    if p_mouse_position is not None:
        direction = angleToVector(p_angle)
        direction = Vector(direction.x / precision, direction.y / precision)

        startPosition = Vector(p_mouse_position.x, p_mouse_position.y)

        newPosition = p_mouse_position
        newPosition.x += direction.x
        newPosition.y += direction.y
        while not p_board.isHere(newPosition) and inBounds(newPosition, p_windowSize):
            newPosition.x += direction.x
            newPosition.y += direction.y

        else:
            return [startPosition, newPosition]
