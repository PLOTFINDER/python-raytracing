import pygame


def renderBoard(window, board):
    window.fill((0, 0, 0))
    cells = board.getCells()
    cellSize = board.getCellSize()
    for y in range(len(cells)):
        for x in range(len(cells[0])):

            positionX = x * cellSize
            positionY = y * cellSize

            if cells[y][x] % 2 == 1:
                pygame.draw.rect(window, "white", (positionX, positionY, cellSize, cellSize))


def renderRays(window, rays):
    for ray in rays:
        startPos = (ray[0].x, ray[0].y)
        endPos = (ray[1].x, ray[1].y)
        pygame.draw.line(window, "grey", startPos, endPos)
