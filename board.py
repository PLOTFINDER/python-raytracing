from vector import Vector
import random


class Board:
    __cellList = None
    __boardSize = None
    __cellSize = None

    def __init__(self, p_boardSize: Vector, p_cellSize: int):
        self.__boardSize = Vector(p_boardSize.x, p_boardSize.y)
        self.__cellSize = p_cellSize
        self.__cellList = [[0 for i in range(p_boardSize.x)] for j in range(p_boardSize.y)]

        for i in range(12):
            x = random.randint(0, p_boardSize.x-1)
            y = random.randint(0, p_boardSize.y-1)
            self.__cellList[y][x] = 1


    def getCells(self):
        return self.__cellList

    def getCellSize(self) -> int:
        return self.__cellSize

    def toListIndex(self, p_vector: Vector) -> tuple:
        x = p_vector.x // self.__cellSize
        y = p_vector.y // self.__cellSize
        return int(x), int(y)

    def isHere(self, p_position: Vector) -> bool:
        x, y = self.toListIndex(p_position)
        try:
            return self.__cellList[y][x] != 0
        except Exception:
            return False
