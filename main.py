import time

from pygame.locals import *

from ray import *
from renderer import *

# CONST VARIABLES
WINDOW_SIZE = (600, 600)
BOARD_SIZE = Vector(10, 10)
CELL_SIZE = 60
RAY_RANGE = (0, 360)
ANGLE_PRECISION = 1
RAY_PRECISION = 1

# VARIABLES
mouse_pos = None
ray = []

# WINDOW INIT
pygame.init()

window = pygame.display.set_mode(WINDOW_SIZE)
pygame.display.set_caption("Ray Casting")
mainClock = pygame.time.Clock()

simulation = True

# SETTINGS
framerate = 60
time_per_frame = 1 / 60
last_time = time.time()

frameCount = 0

# BOARD
board = Board(BOARD_SIZE, CELL_SIZE)

while simulation:
    delta_time = time.time() - last_time
    while delta_time < time_per_frame:
        delta_time = time.time() - last_time

        for event in pygame.event.get():

            if event.type == QUIT:
                simulation = False

    mouse_pos = Vector(pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1])

    rays = calculateNRays(WINDOW_SIZE, board, mouse_pos, RAY_RANGE, ANGLE_PRECISION, RAY_PRECISION)

    renderBoard(window, board)
    renderRays(window, rays)
    pygame.display.update()
    last_time = time.time()
    frameCount += 1
